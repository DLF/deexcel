# deexcel
Simple Python script that turns all sheets in an Excel file (xlsx) into CSV files.

Needs Python 3.

To install, run `pip install git+https://gitlab.com/DLF/deexcel@master#egg=deexcel`.

## Usage
Run `deexcel some_excel.xlsx` to create a CSV file for each sheet within that Excel workbook in the working directory.

