#!/usr/bin/env python
from setuptools import setup

setup(
    name="deexcel",
    version="0.0.2",
    description="Turns Excel into more useful things.",
    author="Daniel Llin Ferrero",
    author_email="daniel@llin.info",
    license="GPL2",
    url="https://gitlab.com/dlf/deexcel",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7"
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    packages=[
        "deexcel",
    ],
    install_requires=[
        "openpyxl",
        "argparse",
        "pyyaml"
    ],
    entry_points="""
    [console_scripts]
    deexcel=deexcel:main
    """,
)
