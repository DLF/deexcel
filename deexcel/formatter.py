from abc import ABC, abstractmethod
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl import Workbook


class Sheet:
    def __init__(self, ws: Worksheet):
        self._ws = ws

    @property
    def rows(self):
        for row in self._ws.rows:
            yield [c.value for c in row]


class Excel:
    def __init__(self, wb: Workbook):
        self._wb: Workbook = wb

    @property
    def sheet_names(self):
        return self._wb.sheetnames

    def get_sheet(self, name: str):
        return Sheet(self._wb.get_sheet_by_name(name))


class Formatter(ABC):
    @abstractmethod
    def export_sheet(self, sheet_name, sheet):
        pass

    def export(self, excel: Excel):
        for sheet_name in excel.sheet_names:
            sheet = excel.get_sheet(sheet_name)
            self.export_sheet(sheet_name, sheet)
