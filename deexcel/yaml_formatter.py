import yaml
from deexcel.formatter import Formatter


class YAMLFormatter(Formatter):

    def __init__(self, use_id):
        self.use_id = use_id

    def export_sheet(self, sheet_name, sheet):
        filename = sheet_name + ".yaml"
        table = [r for r in sheet.rows]
        col_names = table[0]
        if self.use_id:
            data = {}
            for ix, row in enumerate(table[1:]):
                id = row[0]
                if id in data:
                    print("Sheet '{}' will be skipped.".format(sheet_name))
                    print("  Reason: Id {} appears twice. First column must have unique values for YAML export.".format(id))
                    print("  It appears the second time in data row {}.".format(ix))
                    return
                section = {}
                for name, cell in zip(col_names[1:], row[1:]):
                    section[name] = cell
                data[id] = section
        else:
            data = []
            for row in table[1:]:
                section = {}
                for name, cell in zip(col_names, row):
                    section[name] = cell
                data.append(section)

        with open(filename, 'w') as f:
            yaml.dump(data, f)

