import argparse
from openpyxl import load_workbook
from deexcel.csv_formatter import CSVFormatter
from deexcel.yaml_formatter import YAMLFormatter
from deexcel.formatter import Excel

formatters = {
    "csv": CSVFormatter(separator=","),
    "csv-semi": CSVFormatter(separator=";"),
    "yaml": YAMLFormatter(use_id=False),
    "yaml-id": YAMLFormatter(use_id=True),
}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f', '--format', default="csv",
        help='target format',
        choices=formatters.keys()
    )
    parser.add_argument('file', nargs=1)
    args = parser.parse_args()
    wb = load_workbook(filename=args.file[0])
    excel = Excel(wb)
    formatter = formatters[args.format]
    formatter.export(excel)


if __name__ == "__main__":
    main()
