import csv
from deexcel.formatter import Formatter


class CSVFormatter(Formatter):

    def __init__(self, separator=','):
        self.separator = separator

    def export_sheet(self, sheet_name, sheet):
        filename = sheet_name + ".csv"
        with open(filename, 'w') as csvfile:
            # creating a csv writer object
            csvwriter = csv.writer(csvfile, delimiter=self.separator)

            # writing the data rows
            csvwriter.writerows(sheet.rows)


